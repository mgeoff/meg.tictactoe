#pragma once
#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe
{
private:
	//fields
	char m_board[10];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;


public:

	//constructor
	TicTacToe() 
	{
		for (int i = 0; i < 10; i++)
		{
			m_board[i] = '-';
		}
		m_numTurns = 1;
		m_playerTurn = 'X';
		m_winner = ' ';
		
	}

	//methods
	void DisplayBoard()
	{
		cout << m_board[0] << " | " << m_board[1] << " | " << m_board[2] << "\n" <<
			m_board[3] << " | " << m_board[4] << " | " << m_board[5] << "\n" <<
			m_board[6] << " | " << m_board[7] << " | " << m_board[8] << "\n";

	}

	bool IsOver()
	{
		//Horisontal lines X wins
		if (m_board[0] == 'X' && m_board[1] == 'X' && m_board[2] == 'X')
		{
			m_winner = 'X';
			return true;
		}
		if (m_board[3] == 'X' && m_board[4] == 'X' && m_board[5] == 'X')
		{
			m_winner = 'X';
			return true;
		}
		if (m_board[6] == 'X' && m_board[7] == 'X' && m_board[8] == 'X')
		{
			m_winner = 'X';
			return true;
		}
		//Vertical lines X wins
		if (m_board[0] == 'X' && m_board[3] == 'X' && m_board[6] == 'X')
		{
			m_winner = 'X';
			return true;
		}
		if (m_board[1] == 'X' && m_board[4] == 'X' && m_board[7] == 'X')
		{
			m_winner = 'X';
			return true;
		}
		if (m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X')
		{
			m_winner = 'X';
			return true;
		}
		//Diagonals X wins
		if (m_board[6] == 'X' && m_board[4] == 'X' && m_board[2] == 'X')
		{
			m_winner = 'X';
			return true;
		}
		if (m_board[0] == 'X' && m_board[4] == 'X' && m_board[8] == 'X')
		{
			m_winner = 'X';
			return true;
		}

		//Horisontal lines O wins
		if (m_board[0] == 'O' && m_board[1] == 'O' && m_board[2] == 'O')
		{
			m_winner = 'O';
			return true;
		}
		if (m_board[3] == 'O' && m_board[4] == 'O' && m_board[5] == 'O')
		{
			m_winner = 'O';
			return true;
		}
		if (m_board[6] == 'O' && m_board[7] == 'O' && m_board[8] == 'O')
		{
			m_winner = 'O';
			return true;
		}
		//Vertical lines O wins
		if (m_board[0] == 'O' && m_board[3] == 'O' && m_board[6] == 'O')
		{
			m_winner = 'O';
			return true;
		}
		if (m_board[1] == 'O' && m_board[4] == 'O' && m_board[7] == 'O')
		{
			m_winner = 'O';
			return true;
		}
		if (m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O')
		{
			m_winner = 'O';
			return true;
		}
		//Diagonals O wins
		if (m_board[6] == 'O' && m_board[4] == 'O' && m_board[2] == 'O')
		{
			m_winner = 'O';
			return true;
		}
		if (m_board[0] == 'O' && m_board[4] == 'O' && m_board[8] == 'O')
		{
			m_winner = 'O';
			return true;
		}

		if (m_numTurns == 10)
		{
			cout << "The Game Ends In A Tie...\n ";
			return true;
		}

		else return false;
	}

	char GetPlayerTurn()
	{
		if (m_playerTurn == 'X') return 'X';
		else return 'O';

	}

	bool IsValidMove(int position)
	{
		if (0< position && position<10) //first check if its even a 1-9
		{//then check if it's still a space (-1 because array index starts at 0)
			if (m_board[position - 1] == '-')
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else return false;
	}

	void Move(int position)
	{
		
		if (IsValidMove(position))
		{//-1 because array index starts at 0
			m_board[position - 1] = GetPlayerTurn(); //put the x or o in place

			m_numTurns++;
			if (m_numTurns % 2 == 0)
			{
				m_playerTurn = 'O';
			}
			else m_playerTurn = 'X';

		}


	}

	void DisplayResult()
	{
		//say who won
		if (IsOver() && m_winner != ' ') cout << m_winner << " Wins The Game! \n";

		
	}


};